<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home');
})->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/solicitations', 'SolicitationController@index')->name('solicitations.index');
Route::get('/solicitations/create', 'SolicitationController@create')->name('solicitations.create');
Route::post('/solicitations', 'SolicitationController@store')->name('solicitations.store');
Route::get('/solicitations/{solicitation}', 'SolicitationController@show')->name('solicitations.show');
Route::get('/solicitations/{solicitation}/edit', 'SolicitationController@edit')->name('solicitations.edit');
Route::post('/solicitations/{solicitation}', 'SolicitationController@update')->name('solicitations.update');
Route::get('/solicitations/{solicitation}/destroy', 'SolicitationController@destroy')->name('solicitations.destroy');