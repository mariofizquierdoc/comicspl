@extends('admin.layouts.app')

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<h1>
					Solicitations
					<a href="{{ route('solicitations.create') }}" class="btn btn-primary btn-round">Add new</a>
				</h1>


				<hr>

				@forelse ($solicitations as $solicitation)
				<a href="{{ route('solicitations.show', $solicitation) }}">
					<div @if ($solicitation->isActive()) class="alert alert-success" data-toggle="tooltip" data-placement="top" title="Active" @else class="alert alert-primary" @endif>
						{{ $solicitation->monthForHumans() . ' ' . $solicitation->year }}
					</div>
				</a>
				@empty
				There are no solicitations in the system!
				@endforelse


			</div>
		</div>
	</div>
@endsection