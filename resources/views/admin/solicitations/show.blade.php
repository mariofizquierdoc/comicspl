@extends('admin.layouts.app')

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<h1>
					Solicitation for {{ $solicitation->monthForHumans() . ' ' . $solicitation->year }}
					<a href="{{ route('solicitations.edit', $solicitation) }}" class="btn btn-primary btn-round">Edit</a>
					<button type="button" class="btn btn-danger btn-round" data-toggle="modal" data-target="#deleteModal">Delete</button>
				</h1>

				@if ($solicitation->isActive())
					<h3>This is the currently active solicitation!</h3>
				@endif
	
				<hr>

				<p>The solicited products listed here will come out on {{ $solicitation->monthForHumans() . ' ' . $solicitation->year }}</p>

				<p>The pull list for this solicitation @if ($solicitation->isOverdue()) was due at @else must be submitted until @endif {{ $solicitation->due_at->toCookieString() }}</p>
				
			</div>
		</div>
	</div>

	<div class="modal fade" tabindex="-1" role="dialog" id="deleteModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Delete solicitation</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Are you sure you want to <strong>DELETE</strong> this solicitation and its submitted pull lists altogether?</p>
				</div>
				<div class="modal-footer">
					<a href="{{ route('solicitations.destroy', $solicitation) }}" class="btn btn-danger">Do it!</a>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Hell no!</button>
				</div>
			</div>
		</div>
	</div>
@endsection