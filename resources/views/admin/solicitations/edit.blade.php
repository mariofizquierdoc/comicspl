@extends('admin.layouts.app')

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<h1>Edit the solicitation</h1>

				<hr>

				<form method="POST" action="{{ route('solicitations.update', $solicitation) }}">
					@csrf
					<div class="form-group mt-4">
						<label for="inputMonth">Enter month</label>
						<select name="month" id="inputMonth" class="form-control">
							<option value="">Enter month</option>
							@foreach ($months as $value => $month)
								<option value="{{ $value }}" @if ($solicitation->month == $value) selected @endif>{{ $month }}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group mt-4">
						<label for="inputYear">Enter year</label>
						<select name="year" id="inputYear" class="form-control">
							<option value="">Enter year</option>
							@foreach ($years as $year)
								<option value="{{ $year }}" @if ($solicitation->year == $year) selected @endif>{{ $year }}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group mt-4">
						<label for="inputDueAt">Enter due date</label>
						<input name="due_at" type="date" class="form-control" id="inputDueAt" value="{{ $solicitation->due_at->toDateString() }}">
					</div>

					<div class="form-check mt-4">
						<label class="form-check-label" for="inputActive" data-toggle="tooltip" data-placement="right" title="This would be the currently active solicitation">
							<input name="active" type="checkbox" class="form-check-input" id="inputActive" @if ($solicitation->isActive()) checked @endif>
							<span class="form-check-sign"><span class="check"></span></span>
							Active solicitation?
						</label>
					</div>

					<div class="form-group mt-4">
						<button name="submit" type="submit" class="btn btn-primary">Submit</button>
					</div>
				</form>

				@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
		</div>
	</div>
@endsection