<div class="sidebar" data-color="purple" data-background-color="black" data-image="{{ asset('img/sidebar-2.jpg') }}">
    <div class="logo">
    	<a href="#" class="simple-text logo-normal">ComicSPL</a>
    </div>
	<div class="sidebar-wrapper">
		<ul class="nav">
			<li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
					<i class="material-icons">account_circle</i>
                    <p>{{ Auth::user()->name }} <span class="caret"></span></p>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
			<li class="nav-item {{ $dashboardActive }}">
				<a href="{{ route('home') }}" class="nav-link">
					<i class="material-icons">dashboard</i>
					<p>Dashboard</p>
				</a>
			</li>
			<li class="nav-item {{ $clientsActive }}">
				<a href="#" class="nav-link">
					<i class="material-icons">people</i>
					<p>Clients</p>
				</a>
			</li>
			<li class="nav-item {{ $pullListsActive }}">
				<a href="#" class="nav-link">
					<i class="material-icons">recent_actors</i>
					<p>Requests</p>
				</a>
			</li>
			<li class="nav-item {{ $inventoryActive }}">
				<a href="#" class="nav-link">
					<i class="material-icons">import_contacts</i>
					<p>Inventory</p>
				</a>
			</li>
			<li class="nav-item {{ $solicitationsActive }}">
				<a href="{{ route('solicitations.index') }}" class="nav-link">
					<i class="material-icons">content_paste</i>
					<p>Solicitations</p>
				</a>
			</li>
		</ul>
	</div>
</div>