@extends('web.layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    <p>Welcome {{ Auth::user()->name }}!</p>
                    <p>Current solicitation: </p>
                    <p>Arrived recently:</p>
                    <ul>
                        @forelse ($products as $product)
                        <li>{{ $product->name }}</li>
                        @empty
                        <li>Nothing has arrived :(</li>
                        @endforelse
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
