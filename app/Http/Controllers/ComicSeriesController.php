<?php

namespace App\Http\Controllers;

use App\ComicSeries;
use Illuminate\Http\Request;

class ComicSeriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ComicSeries  $comicSeries
     * @return \Illuminate\Http\Response
     */
    public function show(ComicSeries $comicSeries)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ComicSeries  $comicSeries
     * @return \Illuminate\Http\Response
     */
    public function edit(ComicSeries $comicSeries)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ComicSeries  $comicSeries
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ComicSeries $comicSeries)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ComicSeries  $comicSeries
     * @return \Illuminate\Http\Response
     */
    public function destroy(ComicSeries $comicSeries)
    {
        //
    }
}
