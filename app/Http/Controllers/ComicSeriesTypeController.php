<?php

namespace App\Http\Controllers;

use App\ComicSeriesType;
use Illuminate\Http\Request;

class ComicSeriesTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ComicSeriesType  $comicSeriesType
     * @return \Illuminate\Http\Response
     */
    public function show(ComicSeriesType $comicSeriesType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ComicSeriesType  $comicSeriesType
     * @return \Illuminate\Http\Response
     */
    public function edit(ComicSeriesType $comicSeriesType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ComicSeriesType  $comicSeriesType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ComicSeriesType $comicSeriesType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ComicSeriesType  $comicSeriesType
     * @return \Illuminate\Http\Response
     */
    public function destroy(ComicSeriesType $comicSeriesType)
    {
        //
    }
}
