<?php

namespace App\Http\Controllers;

use App\PullList;
use Illuminate\Http\Request;

class PullListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PullList  $pullList
     * @return \Illuminate\Http\Response
     */
    public function show(PullList $pullList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PullList  $pullList
     * @return \Illuminate\Http\Response
     */
    public function edit(PullList $pullList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PullList  $pullList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PullList $pullList)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PullList  $pullList
     * @return \Illuminate\Http\Response
     */
    public function destroy(PullList $pullList)
    {
        //
    }
}
