<?php

namespace App\Http\Controllers;

use App\Solicitation;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class SolicitationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $solicitations = Solicitation::inDescendentOrder()->get();

        return view('admin.solicitations.index', compact('solicitations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $months = \App\Solicitation::MONTHS;

        $currentYear = intval(date('Y'));
        $years = [$currentYear - 1, $currentYear, $currentYear + 1];

        return view('admin.solicitations.create', compact('months', 'years'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $month = $request->get('month');

        $request->validate([
            'month' => 'required|min:1|max:12',
            'year' => Rule::unique('solicitations')->where(function ($query) use ($month) {
                return $query->where('month', $month);
            }),
            'due_at' => 'required|unique:solicitations',
        ]);

        $solicitation = new \App\Solicitation();

        $solicitation->month = $request->get('month');
        $solicitation->year = $request->get('year');
        $solicitation->due_at = $request->get('due_at');
        $solicitation->active = false;

        if ($request->get('active') ?? false) {
            $solicitations = Solicitation::all();

            foreach ($solicitations as $each) {
                $each->active = false;
                $each->save();
            }

            $solicitation->active = true;
        }

        $solicitation->save();

        return redirect()->route('solicitations.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Solicitation  $solicitation
     * @return \Illuminate\Http\Response
     */
    public function show(Solicitation $solicitation)
    {
        return view('admin.solicitations.show', compact('solicitation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Solicitation  $solicitation
     * @return \Illuminate\Http\Response
     */
    public function edit(Solicitation $solicitation)
    {
        $months = \App\Solicitation::MONTHS;

        $currentYear = intval(date('Y'));
        $years = [$currentYear - 1, $currentYear, $currentYear + 1];

        return view('admin.solicitations.edit', compact('solicitation', 'months', 'years'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Solicitation  $solicitation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Solicitation $solicitation)
    {
        $month = $request->get('month');

        $request->validate([
            'month' => 'required|min:1|max:12',
            'year' => Rule::unique('solicitations')->where(function ($query) use ($month) {
                return $query->where('month', $month);
            })->ignore($solicitation->id),
            'due_at' => ['required', Rule::unique('solicitations')->ignore($solicitation->id)],
        ]);
        
        $solicitation->month = $request->get('month');
        $solicitation->year = $request->get('year');
        $solicitation->due_at = $request->get('due_at');
        $solicitation->active = false;

        if ($request->get('active') ?? false) {
            $solicitations = Solicitation::all();

            foreach ($solicitations as $each) {
                $each->active = false;
                $each->save();
            }

            $solicitation->active = true;
        }

        $solicitation->save();

        return redirect()->route('solicitations.show', $solicitation);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Solicitation  $solicitation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Solicitation $solicitation)
    {
        $solicitation->delete();
        return redirect()->route('solicitations.index');
    }
}
