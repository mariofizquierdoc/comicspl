<?php

namespace App\Http\Controllers;

use App\BookSeriesType;
use Illuminate\Http\Request;

class BookSeriesTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BookSeriesType  $bookSeriesType
     * @return \Illuminate\Http\Response
     */
    public function show(BookSeriesType $bookSeriesType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BookSeriesType  $bookSeriesType
     * @return \Illuminate\Http\Response
     */
    public function edit(BookSeriesType $bookSeriesType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BookSeriesType  $bookSeriesType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BookSeriesType $bookSeriesType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BookSeriesType  $bookSeriesType
     * @return \Illuminate\Http\Response
     */
    public function destroy(BookSeriesType $bookSeriesType)
    {
        //
    }
}
