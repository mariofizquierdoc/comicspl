<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Builder;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    public function hasAdminRole()
    {
        $adminRoles = $this->roles()->where('roles.name', 'administrator')->get();
        return !$adminRoles->isEmpty();
    }

    public static function scopeWithAdminRole(Builder $query)
    {
        return $query->whereHas('roles', function($query) {
            return $query->where('roles.name', 'administrator');
        });
    }
}
