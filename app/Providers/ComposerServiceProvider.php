<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::share('dashboardActive', '');
        View::share('clientsActive', '');
        View::share('pullListsActive', '');
        View::share('inventoryActive', '');
        View::share('solicitationsActive', '');

        View::composer('admin.home', function ($view) {
            $dashboardActive = 'active';

            $view->with(compact('dashboardActive'));
        });

        View::composer('admin.solicitations.*', function ($view) {
            $solicitationsActive = 'active';

            $view->with(compact('solicitationsActive'));
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
