<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Solicitation extends Model
{
	const MONTHS = [1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June',
	7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December'];

    protected $fillable = ['month', 'year', 'due_at', 'active'];

    protected $dates = ['due_at'];

    public static function scopeActive(Builder $query)
    {
    	return $query->where('active', 1);
    }

    public static function scopeInDescendentOrder(Builder $query)
    {
    	return $query->orderBy('year', 'desc')->orderBy('month', 'desc');
    }

    public function isActive()
    {
    	return $this->active == true;
    }

    public function monthForHumans()
    {
    	return self::MONTHS[$this->month];
    }

    public function isOverdue()
    {
    	return !$this->due_at->diffAsCarbonInterval(null, false)->invert;
    }
}
